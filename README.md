# System

## Studio dator

### Ljudsystem

#### Pipewire

[PipeWire Docs](https://docs.pipewire.org/index.html)

[PipeWire Tutorial](https://docs.pipewire.org/page_tutorial.html)

[mikeroyal/PipeWire-Guide](https://github.com/mikeroyal/PipeWire-Guide)

[Ubuntu Pipewire WIKI](https://wiki.ubuntu.com/DesktopTeam/TestPlans/Pipewire)
Not very interesting.

[PipeWire configuration](https://docs.pipewire.org/page_daemon.html)

[PipeWire WIKI] (https://gitlab.freedesktop.org/pipewire/pipewire
)

### Boot

Disable boot on some disks

Find UUID of partition `lsblk -f`
In `/etc/default/grub`

add a line: `GRUB_OS_PROBER_SKIP_LIST="<UUID>@/dev/sd<xn>"

Run `sudo update-grub`

## Mount aluback

`sudo apt install cifs-utils`



Make file `~/.smbcredentials` containing:
```
username=<name>
password=<passwd>
```

`~/.smbcredentials` must be own by root and accessabe only by root.

mkdir <dir> in /mnt

Example:
```
//192.168.2.20/public /mnt/<dir> cifs credentials=/home/plu/.smbcredentials,rw,iocharset=utf8,uid=plu 0 0
```
